# Mattock

A simple tmux automation tool written in Rust.

## Installation

Mattock is written in [Rust](https://www.rust-lang.org/en-US/), thus you don't need ot install Ruby or Python. Mattock is distributed as a binary.

1. Go to the [tags](https://gitlab.com/mehemken/mattock/tags) page of this repo.
1. Find the latest version and copy the link.
1. `wget <link>`
1. `chmod a+x mattock`
1. Move mattock to a directory that is in your `PATH`. A sensible place to put Mattock is `~/bin/mattock`. But any directory in your `$PATH` will do.  

To check if you've done it right you can do something like this.

```
$ which mattock
/home/<user>/bin/mattock

$ mattock --version
mattock 0.1.2
```

## Note on convenience

For those, like me, who don't like to type long CLI commands, I recommend you alias or symlink the mattock binary to `mx`.

## Why Mattock?

1. Most other tmux automation tools go overboard. Simple folk don't need 20 sessions with 9 windows and 15 panes each. Just because you can doesn't always mean you should.
1. Most other tmux automation tools require some runtime like Ruby or Python¹. Mattock is written in Rust and does not require Ruby or Python. 

## Why not Mattock?

1. It is tailored to the workflow of some guy on the internet who uses vim (neovim actually) [with no plugins](../docs/no-plugins.jpg).
1. It has no buzz.
1. It is not written in Python.
1. It is not written in Ruby.
1. You can't do crazy windowing/pane stuff.

## Basic idea

So, let's say you find yourself starting the same tmux sessions on a daily basis. Typing the following gets boring quick. Not o mention, what if you forget where your directory went?

```
$ tmux new -s foo -c ~/Code/projects/bighairy/the-build-part/the-component-i-work-on/the-code-bit
```

Ok so that's one project. You're a tidy dev and you like to keep stuff separate. You have five other projects you work on that each require a tmux session similar to the one above.

Enter mattock.

![`mattock`](../docs/mattock.jpg).

Mattock is not a chainsaw, or a lawn mower. It is not a tractor or a [Big Bud](../docs/big-bud.jpg). It is a hand held tool that's useful for a few things.

So how does mattock help?


```
➜  mx ls
··ask     ~/Documents/gitlab/ask
··gopl    ~/go/src/gopl/ch7
··rs      ~/rust/projects/
··tmp     ~/Documents/tmp

➜  mx add rs
New session created ∙∙rs ~/rust/projects/

➜  mx add tmp
New session created ∙∙tmp ~/Documents/tmp

➜  tmux ls
rs: 1 windows (created Wed Oct 17 08:32:40 2018) [80x24]
tmp: 1 windows (created Wed Oct 17 08:32:44 2018) [80x24]

➜  mx rm tmp
Session deleted ∙∙tmp ~/Documents/tmp

➜  mx rm rs
Session deleted ∙∙rs ~/rust/projects/

➜  tmux ls
no server running on /tmp/tmux-1000/default
```

So the tmux sessions available to you using the `mx ls` command are 100% certified configurable. All you have to do is add a file at ` ~/.config/mattock.yaml` and add make it look like this:

```
default:
  - name: tmp
    dir:  "~/Documents/tmp"
  - name: py
    dir:  "~/Documents/python/src"
  - name: rs
    dir: "~/rust/projects/"
library:
  - name: ask
    dir:  "~/Documents/gitlab/ask
  - name: gopl
    dir:  "~/go/src/gopl/ch7"
  - name: rs
    dir: "~/rust/projects/"
  - name: mattock
    dir: "~/rust/projects/mattock/mattock"
  - name: tmp
    dir:  "~/Documents/tmp"
```

Wow that's amazing. Really, stupendous. Quite nearly groundbreaking. So from here, there are two things you can do depending on your current situation. You might find yourself already inside a tmux session. How do you switch to this new one? Easy, `C-b s` and then `jkjkj<enter>` until you get to the one you want.

You may, however, find yourself at the start of your day, in a non-multiplexed terminal situation (uniplexed?). Yuck. In this case just `tmux a` or maybe `tmux a -t rs`. No Mattock command exists for that because that's already very concise.

And they lived hapily ever after.

## Default sessions

The default sessions can be created all at once by invoking `mattock def`. These are your daily sessions.

The tmux sessions created will be equivalent to

    $ tmux new -ds <name> -c <dir>

---

¹ I happen to be a Python guy and the few times I've had to use Ruby I became very angry at its versioning. Perhaps Ruby folk feel similar about the Pythons.
