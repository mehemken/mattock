#[macro_use]
extern crate clap;
extern crate mattock;

use clap::App;
use mattock::{Config, run};
use std::process;

////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////

fn main() {
    // CLAP: Argument parsing
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    // mattock: ~/mattock.yaml
    let config = match Config::new() {
        Ok(v)  => v,
        Err(e) => {
            println!("could not load config: {:?}", e);
            process::exit(1);
        }
    };

    // Execute the run function
    if let Err(e) = run(matches, config) {
        println!("application error: {}", e);
        process::exit(1);
    }

}
