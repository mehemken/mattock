#[macro_use]
extern crate serde_derive;

extern crate clap;
extern crate dirs;

extern crate serde;
extern crate serde_yaml;

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::process::Command;
use std::process;
use std::path::PathBuf;
use std::env;


////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////

// --- Config ---

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub default: Vec<Session>,
    pub library: Vec<Session>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Session {
    pub name: String,
    pub dir:  String,
}

impl Config {
    // new loads the config file at ~/.config/mattock.yaml
    pub fn new() -> Result<Config, Box<Error>> {
        let mut path = PathBuf::new();
        let home = match dirs::config_dir() {
            Some(d) => d,
            None => {
                println!("error finding ~/.config");
                process::exit(1);
            },
        };
        path.push(home);
        path.push("mattock.yaml");
        let file = File::open(path)?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)
            .expect("failed to read_to_string");
        let deserialized: Config = serde_yaml::from_str(&contents)
            .unwrap();
        Ok(deserialized)
    }
}


////////////////////////////////////////////////////////////
// Funcs
////////////////////////////////////////////////////////////

// run defines the program logic after command line flags
// and configuration files are read.
pub fn run(matches: clap::ArgMatches, config: Config) -> Result<(), Box<Error>> {

    // Looking for the ls command

    if let Some(_matches) = matches.subcommand_matches("ls") {
        show_cmd(&config.library);
        // Use matches here to handle options to the subcommand "ls"
        // ...
    }

    // Looking for the def command

    if let Some(_matches) = matches.subcommand_matches("def") {
        create_sessions(&config.default)?;
        // Use matches here to handle options to the subcommand "ls"
        // ...
    }

    // Looking for the add command and open library session by name

    if let Some(matches) = matches.subcommand_matches("add") {
        if let Some(sesh) = matches.value_of("name") {
            match find_session(sesh, config.library.clone()) {
                Ok(s)  => {
                    create_sessions(&s)?;
                },
                Err(e) => println!("error: {}", e),
            }
        }
    }

    // Delete an active library session by name

    if let Some(matches) = matches.subcommand_matches("rm") {
        if let Some(name) = matches.value_of("name") {
            match delete_daemon_session(name) {
                Ok(()) => println!("Session deleted: {}", name),
                Err(e) => println!("error: {}", e),
            };
        }
    }
    Ok(())
}


// show_cmd takes a &Vec<Sessions> and prints a human readable description
// of each session.
fn show_cmd(sessions: &Vec<Session>) {
    let mut name_width = 0;
    for sesh in sessions {
        if sesh.name.len() > name_width {
            name_width = sesh.name.len()
        }
    }
    for sesh in sessions {
        println!("··{:width$} {}", sesh.name, sesh.dir, width=name_width);
    }
}


// expand_user replaces a ~ with /home/<user>, allowing the use of ~
// in the ~/.config/mattock.yaml declarations.
fn expand_user(d: &String) -> Result<String, &'static str> {
    let split = match d.find("~") {
        Some(x) => x,
        None    => d.len() + 1,
    };
    if split > d.len() {
        return Ok(d.to_owned())
    }
    let user = env::var("USER").unwrap();
    let home = format!("/home/{}", user);
    Ok(d.replace("~", &home))
}


// create_sessions takes a reference to a vector of sessions and
// calls spawn_daemon_session on each Session.
fn create_sessions(s: &Vec<Session>) -> Result<(), Box<Error>> {
    for sesh in s {
        if let Err(e) = spawn_daemon_session(&sesh) {
            println!("error creating session {:?}: {}", &sesh, e);
        }
    }
    Ok(())
}


// find_session takes ownership of the library and returns a vector
// whose only session is the session that was found. Else it returns
// an error.
fn find_session(s: &str, lib: Vec<Session>) -> Result<Vec<Session>, &'static str> {
    for sesh in lib {
        if sesh.name == s {
            return Ok(vec!(sesh))
        }
    }
    Err("session not found")
}


// spawn_daemon_session takes a single Session struct
// and creates a new tmux session in the backgroud.
fn spawn_daemon_session(s: &Session) -> Result<(), Box<Error>> {
    let expanded = expand_user(&s.dir)?;
    let mut cmd = Command::new("tmux");
    cmd.args(&["new", "-ds", &s.name, "-c", &expanded]);
    println!("New session created ∙∙{} {}", s.name, s.dir);
    cmd.spawn()?;
    Ok(())
}


// Sends a kill-session command to tmux.
fn delete_daemon_session(s: &str) -> Result<(), Box<Error>> {
    let mut cmd = Command::new("tmux");
    cmd.args(&["kill-session", "-t", s]);
    cmd.spawn()?;
    Ok(())
}


////////////////////////////////////////////////////////////
// Unit Tests
////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {

    use super::*;
    use std::env;


    ////////////////////////////////////////
    // Tests for expand_user
    ////////////////////////////////////////

    #[test]
    fn expand_user_short_from_tilde() {
        let user        = env::var("USER").unwrap();
        let constructed = format!("/home/{}", user);
        let expanded    = expand_user(&"~".to_string()).unwrap();
        assert_eq!(expanded, constructed);
    }

    #[test]
    fn expand_user_short_from_tilde_slash() {
        let user        = env::var("USER").unwrap();
        let constructed = format!("/home/{}/", user);
        let expanded    = expand_user(&"~/".to_string()).unwrap();
        assert_eq!(expanded, constructed);
    }

    #[test]
    fn expand_user_home_documents() {
        let user        = env::var("USER").unwrap();
        let constructed = format!("/home/{}/Documents", user);
        let expanded    = expand_user(&"~/Documents".to_string()).unwrap();
        assert_eq!(expanded, constructed);
    }

}
